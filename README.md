#Authentication Bundle
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/authentication-bundle/)

## Symfony Bundle

This is a Symfony Bundle to authenticate your users in your symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)

## Installation

### via CLI
Run ``composer require jsp/authentication-bundle:"0.1.*"`` command in cli to install source with Version 0.1

### composer.json
Add following to your symfony application composer json file:
```json
{
  "require": {
    "jsp/authentication-bundle": "0.1.*"
  }
}
```

## Configuration

### Routing
Copy  ``\vendor\jsp\authentication-bundle\Resources\config\routes\jsp-authentication.yaml.dist`` _without_ suffix ``.dist`` into ``\config\routes\`` Folder
 
### Security
Add these information to your security.yaml file

> See JohnSear\JspUserBundle Configuration for User Provider and Encoders 

```yaml
security:
    # ..
    firewalls:
        # ..
        main:
            pattern:  ^/
            anonymous: ~

            guard:
                authenticators:
                    - JohnSear\JspAuthenticationBundle\Security\FormLoginAuthenticator
            logout:
                path: jsp_logout
                # where to redirect after logout
                target: jap_login

            remember_me:
                secret:   '%kernel.secret%'
                lifetime: 604800 # 1 week in seconds
                path:     /
            # ..

# ..
```

### Register all Services

For now, all services must be auto wired via the services.yaml.

Add following Lines at the end of ``config\services.yaml``

```yaml
services:

    # ..

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    JohnSear\JspAuthenticationBundle\:
        resource: '../vendor/jsp/authentication-bundle/*'
# ..
```
