<?php declare(strict_types=1);

namespace JohnSear\JspAuthenticationBundle;

use JohnSear\JspAuthenticationBundle\DependencyInjection\JspAuthenticationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspAuthenticationBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspAuthenticationExtension();
    }
}
